#Landing Page

Project shows dynamic generation of menu. Script behavior:
- at load, it generates random number of sections (from 1 to 5),
- generates menu based of number of sections on page,
- has option to add new sections
- marks current displayed section on the menu.