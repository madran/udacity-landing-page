"use strict";

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function startApp() {

    let numberOfSections = 0;

    let activeSection = null;
    let prevActiveSection = null;

    let sections = null;

    document.addEventListener('scroll', setActiveSection);
    document.addEventListener('scroll', markActiveSectionOnScroll);

    let addSectionButton = document.getElementById('menu__add-section');
    addSectionButton.addEventListener('click', ev => {

        ev.preventDefault();
        addSection();
        setActiveSection();
        markActiveSection();
    });

    generateRandomNumberOfSections();

    function buildMenu() {

        let menuListFragment = document.createDocumentFragment()

        for (let i = 1; i <= numberOfSections; i++) {
            let menuLink = document.createElement('a');
            menuLink.classList.add('menu__link');
            menuLink.setAttribute('href', '#section-' + i);
            menuLink.setAttribute('nr', i);
            menuLink.textContent = 'Section-' + i;
            menuLink.addEventListener('click', ev => {
                ev.preventDefault();
                let activeSectionNr = ev.target.getAttribute('nr');
                let section = document.querySelector('section[nr="' + activeSectionNr + '"]');
                activeSection = prevActiveSection = section;
                section.scrollIntoView({behavior: 'smooth'});
            });

            let menuElement = document.createElement('li');
            menuElement.classList.add('menu__element');
            menuElement.appendChild(menuLink);

            menuListFragment.appendChild(menuElement);
        }

        let menuList = document.getElementById('menu__list');

        menuListFragment.appendChild(menuList.lastElementChild);

        menuList.querySelectorAll('*').forEach(n => n.remove());

        menuList.appendChild(menuListFragment);

        sections = document.getElementsByClassName('section');
    }

    function addSection() {

        let main = document.getElementById('main');
        main.appendChild(createSection());

        numberOfSections = numberOfSections + 1;

        buildMenu();

        //prevent from add more than 9 sections
        if (numberOfSections >= 9) {
            document.getElementById('menu__list').lastElementChild.remove();
        }
    }

    function createSection() {

        let currentNrOfSections = numberOfSections + 1;

        let header = document.createElement('h1');
        header.innerText = 'Header For Important Content ' + currentNrOfSections;
        header.classList.add('section__header');

        let text = document.createElement('p');
        text.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vel neque ut velit efficitur luctus et vel orci. Donec sit amet feugiat tellus, sit amet mollis arcu. Donec lacinia vel neque eget imperdiet. Nunc feugiat, velit eu congue sodales, massa turpis ultrices mi, at tempus magna leo quis ipsum. Donec pharetra laoreet nibh non faucibus. Nam at mattis metus. Praesent auctor augue sem, vel rhoncus felis facilisis vitae.';
        text.classList.add('section__content');

        let shade = document.createElement('div');
        shade.classList.add('section__shade');
        shade.appendChild(header);
        shade.appendChild(text);

        let section = document.createElement('section');
        section.classList.add('section');
        section.appendChild(shade);
        section.id = 'section-' + currentNrOfSections;
        section.setAttribute('nr', currentNrOfSections);

        if (numberOfSections % 2 === 1) {
            section.classList.add('flex-left');
            section.classList.add('section-background-odd');
        } else {
            section.classList.add('flex-right');
            section.classList.add('section-background-even');
        }

        return section;
    }

    function setActiveSection() {

        for (const section of sections) {
            let sectionPosition = section.getBoundingClientRect();
            if (sectionPosition.top <= window.innerHeight * 0.4 && sectionPosition.top > -20) {
                activeSection = section;
            }
        }
    }

    function markActiveSectionOnScroll() {

        if (activeSection !== prevActiveSection) {

            markActiveSection();
        }
    }

    function markActiveSection() {

        let activeSectionNr = activeSection.getAttribute('nr');
        clearActiveState('menu__link--active');
        clearActiveState('section__shade--active');

        document.querySelector(`a[nr="${activeSectionNr}"]`).classList.add('menu__link--active');
        document.querySelector(`section[nr="${activeSectionNr}"]`).firstChild.classList.add('section__shade--active');
        prevActiveSection = activeSection;
    }

    function clearActiveState(className) {

        let prevActiveLinks = document.getElementsByClassName(className);
        for (const prevActiveLink of prevActiveLinks) {
            prevActiveLink.classList.remove(className);
        }
    }

    function generateRandomNumberOfSections() {

        let initialNrOfSections = getRandomIntInclusive(1, 5);
        for (let i = 0; i < initialNrOfSections; i++) {
            addSection();
        }

        setActiveSection();
        markActiveSection();
    }
}

startApp();
